import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Encabezado1Component } from './encabezado1/encabezado1.component';
import { Cuerpo1Component } from './cuerpo1/cuerpo1.component';
import { Otrocuerpo1Component } from './otrocuerpo1/otrocuerpo1.component';

@NgModule({
  declarations: [
    AppComponent,
    Encabezado1Component,
    Cuerpo1Component,
    Otrocuerpo1Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
